package com.ing.starbucks.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ing.starbucks.model.User;

@Repository
public interface userRepository extends JpaRepository<User, Long> {

}