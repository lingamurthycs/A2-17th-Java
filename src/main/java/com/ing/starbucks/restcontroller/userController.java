package com.ing.starbucks.restcontroller;


import com.ing.starbucks.model.User;
import com.ing.starbucks.repository.userRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**

 */
@RestController
@RequestMapping("/api")
public class userController{
	
	//@Autowired
	//User user;
	
    @Autowired
    userRepository userRepository;

    /*@GetMapping("/userValidation")
    public User getUserValidated() {
    	
    	
    	return user;
    }*/
    
    @GetMapping("/user")
    public List<User> getAlluser() {
        return userRepository.findAll();
    }
    
    

    @GetMapping("/user/{id}")
    public ResponseEntity<User> getuserById(@PathVariable(value = "id") Long userId) {
    	User user1 = userRepository.findOne(userId);
        if(user1 == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(user1);
    }

    @PostMapping("/user")
    public User createuser(@Valid @RequestBody User user) {
        return userRepository.save(user);
    }

    @DeleteMapping("/user/{id}")
    public User deleteuser(@PathVariable(value = "id") Long userId) {
        User user = userRepository.findOne(userId);
        if(user == null) {
            return null;//ResponseEntity.notFound().build()
        }
        userRepository.delete(user);
        return user;//ResponseEntity.ok().build()
    }
}