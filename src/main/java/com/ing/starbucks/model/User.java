package com.ing.starbucks.model;


import org.hibernate.validator.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;
@Entity
@Table(name = "user")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, 
        allowGetters = true)
public class User implements Serializable {
	
	    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		@Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    private Long cif;

	    @NotBlank
	    private String deviceId;

	    @NotBlank
	    private String deviceLocation;
	    
	    @NotBlank
	    private Long latiture;
	    
	    @NotBlank
	    private Long longitude;

	    @Column(nullable = false, updatable = false)
	    @Temporal(TemporalType.TIMESTAMP)
	    @CreatedDate
	    private Date timePlaced;

		public Long getCif() {
			return cif;
		}

		public void setCif(Long cif) {
			this.cif = cif;
		}

		public String getDeviceId() {
			return deviceId;
		}

		public void setDeviceId(String deviceId) {
			this.deviceId = deviceId;
		}

		public String getDeviceLocation() {
			return deviceLocation;
		}

		public void setDeviceLocation(String deviceLocation) {
			this.deviceLocation = deviceLocation;
		}

		public Long getLatiture() {
			return latiture;
		}

		public void setLatiture(Long latiture) {
			this.latiture = latiture;
		}

		public Long getLongitude() {
			return longitude;
		}

		public void setLongitude(Long longitude) {
			this.longitude = longitude;
		}

		public Date getTimePlaced() {
			return timePlaced;
		}

		public void setTimePlaced(Date timePlaced) {
			this.timePlaced = timePlaced;
		}

		public static long getSerialversionuid() {
			return serialVersionUID;
		}

	    
}
