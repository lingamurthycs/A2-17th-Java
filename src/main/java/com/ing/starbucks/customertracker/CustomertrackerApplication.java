package com.ing.starbucks.customertracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomertrackerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomertrackerApplication.class, args);
	}
}
